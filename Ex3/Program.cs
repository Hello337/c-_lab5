﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex3
{
    class Program
    {
        public class Ship
        {
            protected string name;
            protected string appointment;
            protected int displacement;
            protected int power;
            protected string typeOfFuel;
            public Ship(string name, string appointment, int displacement, int power, string typeOfFuel)
            {
                this.name = name;
                this.appointment = appointment;
                this.displacement = displacement;
                this.power = power;
                this.typeOfFuel = typeOfFuel;
            }
        }

        public class Carrier : Ship
        {
            protected string typeOfPlane;
            protected int countOfPlane;

            public Carrier(string name, string appointment, int displacement, int power, string typeOfFuel, string typeOfPlane, int countOfPlane) : base(name, appointment, displacement, power, typeOfFuel) {
                this.typeOfPlane = typeOfPlane;
                this.countOfPlane = countOfPlane;
            }

            public void ShowInfo()
            {
                Console.WriteLine(this.name);
                Console.WriteLine(this.appointment);
                Console.WriteLine(this.displacement);
                Console.WriteLine(this.power);
                Console.WriteLine(this.typeOfFuel);
                Console.WriteLine(this.typeOfPlane);
                Console.WriteLine(this.countOfPlane);
            }
        }

        public class RocketLauncher : Ship
        {
            protected string typeOfRocket;
            protected int countOfRocket;

            public RocketLauncher(string name, string appointment, int displacement, int power, string typeOfFuel, string typeOfRocket, int countOfRocket) : base(name, appointment, displacement, power, typeOfFuel)
            {
                this.typeOfRocket = typeOfRocket;
                this.countOfRocket = countOfRocket;
            }

            public void ShowInfo()
            {
                Console.WriteLine(this.name);
                Console.WriteLine(this.appointment);
                Console.WriteLine(this.displacement);
                Console.WriteLine(this.power);
                Console.WriteLine(this.typeOfFuel);
                Console.WriteLine(this.typeOfRocket);
                Console.WriteLine(this.countOfRocket);
            }
        }
        static void Main(string[] args)
        {
            RocketLauncher launcherBalora = new RocketLauncher("Balora", "Launching", 50, 1000, "Diesel", "Nuclear", 200);
            launcherBalora.ShowInfo();
        }
    }
}
