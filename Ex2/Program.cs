﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            A object1 = new A();
            Console.WriteLine(object1.C);
            A object2 = new A(2, 2);
            Console.WriteLine(object2.C);
            B object3 = new B();
            Console.WriteLine(object3.C2);
            B object4 = new B(10, 10, 10);
            Console.WriteLine(object4.C2);
        }
    }
}
