﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2
{
    class A
    {
        protected int a = 15;
        protected int b = 10;
        public int C
        {
            get { return a + b; }
            set { a = value; b = value; }
        }
        public A()
        {
            a = 999;
            b = 999;
        }
        public A(int firstNumber, int secondNumber)
        {
            this.a = firstNumber;
            this.b = secondNumber;
        }
    }
}
