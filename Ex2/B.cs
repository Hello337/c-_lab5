﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2
{
    class B : A
    {
        private int d;
        public int C2
        {
            get { return a + b + d; }
            set { a = value; b = value; d = value; }
        }
        public B()
        {
            a = 999;
            b = 999;
            d = 999;
        }
        public B(int firstNumber, int secondNumber, int thirdNumber) : base(firstNumber, secondNumber)
        {
            this.d = thirdNumber;
        }
    }
}
