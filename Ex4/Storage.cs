﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4
{
    class Storage : IComparable
    {
        private string name;
        private int price;
        private string date;
        private int size;
        private string manufacturer;
        private string sorting;


        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int Price
        {
            get { return price; }
            set { price = value; }
        }
        public string Date
        {
            get { return date; }
            set { date = value; }
        }
        public int Size
        {
            get { return size; }
            set { size = value; }
        }
        public string Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }
        public string Sorting
        {
            get { return sorting; }
            set { sorting = value; }
        }
        public Storage()
        {

        }
         public Storage(string nameOfProduct, int priceOfProduct, string dateOfProduct, int sizeOfProduct, string manufacturerOfProduct, string typeOfSorting)
        {
            this.name = nameOfProduct;
            this.price = priceOfProduct;
            this.date = dateOfProduct;
            this.size = sizeOfProduct;
            this.manufacturer = manufacturerOfProduct;
            this.sorting = typeOfSorting;
        }

        public void InputInfo()
        {
            Console.WriteLine("Введення даних про товар: ");
            Console.WriteLine("Введіть назву товару: ");
            name = Console.ReadLine();
            Console.WriteLine("Введіть ціну товару: ");
            price = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введіть дату надходження товару: ");
            date = Console.ReadLine();
            Console.WriteLine("Введіть розмір товару: ");
            size = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введіть виробника товару: ");
            manufacturer = Console.ReadLine();
            Console.WriteLine("Введіть за яким параметром буде сортування товару: ");
            sorting = Console.ReadLine();
        }
        public void ShowInfo()
        {
            Console.WriteLine("Вивід товару: ");
            Console.WriteLine("Назва товару: " + name);
            Console.WriteLine("Ціна товару: " + price);
            Console.WriteLine("Дата надходження: " + date);
            Console.WriteLine("Розмір товару: " + size);
            Console.WriteLine("Виробник товару: " + manufacturer);

        }
        public void SearchingByDate(string yourDate)
        {
            if (date == yourDate)
                ShowInfo();
        }
        public void SearchingBySize(int yourSize)
        {
            if (size == yourSize)
                ShowInfo();
        }
        public void SearchingByPrice(int yourPrice)
        {
            if (price == yourPrice)
                ShowInfo();
        }
        public int CompareTo(object o)
        {
            Storage obj = o as Storage;

            if (sorting == "name")
                return name.CompareTo(obj.name);
            else if (sorting == "price")
                return price.CompareTo(obj.price);
            else if (sorting == "manufacturer")
                return manufacturer.CompareTo(obj.manufacturer);
            else
            {
                Console.WriteLine("You chose a wrong sorting variant!");
                return 0;
            }
        }
    }
}
