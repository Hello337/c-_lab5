﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfObjects;
            Console.WriteLine("Enter number of objects: ");
            numberOfObjects = Convert.ToInt32(Console.ReadLine());
            Storage[] StorageObjects = new Storage[numberOfObjects];
            for (int i = 0; i < numberOfObjects; i++)
            {
                StorageObjects[i] = new Storage();
            }
            for (int i = 0; i < numberOfObjects; i++)
            {
                StorageObjects[i].InputInfo();

            }
            for (int i = 0; i < numberOfObjects; i++)
            {
                StorageObjects[i].ShowInfo();
            }
            Array.Sort(StorageObjects);
            for (int i = 0; i < numberOfObjects; i++)
            {
                StorageObjects[i].ShowInfo();
            }
            for (int i = 0; i < numberOfObjects; i++)
            {
                StorageObjects[i].SearchingByPrice(100);
            }
        }
    }
}
